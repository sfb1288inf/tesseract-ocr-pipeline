FROM debian:buster-slim


LABEL authors="Patrick Jentsch <p.jentsch@uni-bielefeld.de>"


ARG UID="1000"
ARG GID="1000"


ENV LANG="C.UTF-8"
ENV PYTHONDONTWRITEBYTECODE="1"
ENV PYTHONUNBUFFERED="1"


RUN apt-get update \
 && apt-get install --no-install-recommends --yes \
      autoconf \
      automake \
      build-essential \
      g++ \
      libgif-dev \
      libjpeg62-turbo-dev \
      libopenjp2-7-dev \
      libpng-dev \
      libtiff5-dev \
      libtool \
      libwebp-dev \
      pkg-config \
      poppler-utils \
      python \
      python-tk \
      python-virtualenv \
      python3 \
      python3-venv \
      wget \
      zip \
      zlib1g-dev \
 && rm --recursive /var/lib/apt/lists/*


ENV LEPTONICA_VERSION="1.85.0"
RUN wget \
      --output-document - \
      --quiet "https://github.com/DanBloomberg/leptonica/releases/download/${LEPTONICA_VERSION}/leptonica-${LEPTONICA_VERSION}.tar.gz" \
    | tar -xzf - \
 && cd "leptonica-${LEPTONICA_VERSION}" \
 && ./configure \
 && make \
 && make install \
 && cd - > /dev/null \
 && rm --recursive "leptonica-${LEPTONICA_VERSION}"


ENV TESSERACT_VERSION="5.4.1"
RUN wget \
      --output-document - \
      --quiet "https://github.com/tesseract-ocr/tesseract/archive/${TESSERACT_VERSION}.tar.gz" \
    | tar -xzf - \
 && cd "tesseract-${TESSERACT_VERSION}" \
 && ./autogen.sh \
 && ./configure \
      --disable-openmp \
      --disable-shared \
      "CXXFLAGS=-g -O2 -fno-math-errno -Wall -Wextra -Wpedantic" \
 && make \
 && make install \
 && ldconfig \
 && cd - > /dev/null \
 && rm --recursive "tesseract-${TESSERACT_VERSION}"


RUN groupadd --gid "${GID}" generic \
 && useradd --create-home --gid generic --no-log-init --uid "${UID}" generic
USER generic
WORKDIR /home/generic


ENV PYTHON2_VENV_PATH="/home/generic/python2_venv"
ENV PYTHON3_VENV_PATH="/home/generic/python3_venv"
RUN python2 -m virtualenv "${PYTHON2_VENV_PATH}" \
 && python3 -m venv "${PYTHON3_VENV_PATH}"
ENV PATH="${PYTHON3_VENV_PATH}/bin:${PYTHON2_VENV_PATH}/bin:${PATH}"


ENV PYFLOW_VERSION="1.1.20"
RUN wget \
      --output-document - \
      --quiet "https://github.com/Illumina/pyflow/releases/download/v${PYFLOW_VERSION}/pyflow-${PYFLOW_VERSION}.tar.gz" \
    | tar -xzf - \
 && cd "pyflow-${PYFLOW_VERSION}" \
 && python2 setup.py build install \
 && cd - > /dev/null \
 && rm --recursive "pyflow-${PYFLOW_VERSION}"


ENV OCROPY_VERSION="1.3.3"
RUN wget \
      --output-document - \
      --quiet "https://github.com/ocropus-archive/DUP-ocropy/archive/refs/tags/v${OCROPY_VERSION}.tar.gz" \
    | tar -xzf - \
 && cd "DUP-ocropy-${OCROPY_VERSION}" \
 && python2 -m pip install --requirement requirements.txt \
 && python2 setup.py install \
 && cd - > /dev/null \
 && rm --recursive "DUP-ocropy-${OCROPY_VERSION}"


COPY hocr-combine hocr2tei tesseract-ocr-pipeline /usr/local/bin/


ENTRYPOINT ["tesseract-ocr-pipeline"]
CMD ["--help"]
